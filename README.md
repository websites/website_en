# Duniter website

Public site available at https://duniter.org/en

## Reproduce it locally

You may want to reproduce this website locally, for developement purposes for example. Here are the instructions.

Clone the sources

    git clone https://git.duniter.org/websites/website_en.git

Install python stuff

    cd website_en
    python3 -m venv .venv
    source .venv/bin/activate
    pip install -r requirements.txt

Install system dependencies for plantuml plugin (plantuml and GraphViz utilities) :
```bash
apt install plantuml graphviz
```

Generate the site

    pelican
    # or python3 -m pelican

Serve it

    pelican --listen --autoreload --port 8557 # port is hardcoded in develop conf file

The website should be available at http://localhost:8557.

## Generate production site

You just need to give the production configuration file to Pelican:

    pelican -s publishconf.py

You may want to change the production parameters, like the domain name: just edit `publishconf.py` and modify the `SITEURL` to whatever value you want. You can also override any other value simply by adding it.

For example if you want to host the site at `https://my.website.org`, set:

    SITEURL = u'https://my.website.org'

## Plantuml plugin documentation

 * Plantuml plugin documentation : https://github.com/Scheirle/pelican-plugins/tree/master/plantuml
 * Plantuml documentation: http://plantuml.com
 * Plantuml support DOT language of GraphViz: http://www.graphviz.org/Gallery.php
