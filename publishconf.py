#!/usr/bin/env python
# -*- coding: utf-8 -*- #
import sys
sys.path.append('.')

from pelicanconf import *

SITEURL = u'https://duniter.org'
